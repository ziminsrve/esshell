/*
 * This file is generated by jOOQ.
 */
package org.jooq.util.maven.example.tables.records;


import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.TableRecordImpl;
import org.jooq.util.maven.example.tables.Conclusion;


/**
 * Заключения
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ConclusionRecord extends TableRecordImpl<ConclusionRecord> implements Record3<Long, Long, Integer> {

    private static final long serialVersionUID = -1957085627;

    /**
     * Setter for <code>public.conclusion.rule_id</code>.
     */
    public void setRuleId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.conclusion.rule_id</code>.
     */
    @NotNull
    public Long getRuleId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.conclusion.fact_id</code>.
     */
    public void setFactId(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.conclusion.fact_id</code>.
     */
    @NotNull
    public Long getFactId() {
        return (Long) get(1);
    }

    /**
     * Setter for <code>public.conclusion.order</code>.
     */
    public void setOrder(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.conclusion.order</code>.
     */
    @NotNull
    public Integer getOrder() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Long, Long, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Long, Long, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return Conclusion.CONCLUSION.RULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field2() {
        return Conclusion.CONCLUSION.FACT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return Conclusion.CONCLUSION.ORDER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getRuleId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component2() {
        return getFactId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getOrder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getRuleId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value2() {
        return getFactId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getOrder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConclusionRecord value1(Long value) {
        setRuleId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConclusionRecord value2(Long value) {
        setFactId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConclusionRecord value3(Integer value) {
        setOrder(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConclusionRecord values(Long value1, Long value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ConclusionRecord
     */
    public ConclusionRecord() {
        super(Conclusion.CONCLUSION);
    }

    /**
     * Create a detached, initialised ConclusionRecord
     */
    public ConclusionRecord(Long ruleId, Long factId, Integer order) {
        super(Conclusion.CONCLUSION);

        set(0, ruleId);
        set(1, factId);
        set(2, order);
    }
}
