/*
 * This file is generated by jOOQ.
 */
package org.jooq.util.maven.example.tables.records;


import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.util.maven.example.tables.Fact;


/**
 * Факты
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class FactRecord extends UpdatableRecordImpl<FactRecord> implements Record3<Long, Long, Long> {

    private static final long serialVersionUID = 1582966883;

    /**
     * Setter for <code>public.fact.id</code>. Идентификатор факта
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.fact.id</code>. Идентификатор факта
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.fact.variable_id</code>.
     */
    public void setVariableId(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.fact.variable_id</code>.
     */
    @NotNull
    public Long getVariableId() {
        return (Long) get(1);
    }

    /**
     * Setter for <code>public.fact.domain_value_id</code>.
     */
    public void setDomainValueId(Long value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.fact.domain_value_id</code>.
     */
    @NotNull
    public Long getDomainValueId() {
        return (Long) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Long, Long, Long> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Long, Long, Long> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return Fact.FACT.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field2() {
        return Fact.FACT.VARIABLE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field3() {
        return Fact.FACT.DOMAIN_VALUE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component2() {
        return getVariableId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component3() {
        return getDomainValueId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value2() {
        return getVariableId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value3() {
        return getDomainValueId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FactRecord value1(Long value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FactRecord value2(Long value) {
        setVariableId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FactRecord value3(Long value) {
        setDomainValueId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FactRecord values(Long value1, Long value2, Long value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached FactRecord
     */
    public FactRecord() {
        super(Fact.FACT);
    }

    /**
     * Create a detached, initialised FactRecord
     */
    public FactRecord(Long id, Long variableId, Long domainValueId) {
        super(Fact.FACT);

        set(0, id);
        set(1, variableId);
        set(2, domainValueId);
    }
}
