/*
 * This file is generated by jOOQ.
 */
package org.jooq.util.maven.example;


import javax.annotation.Generated;

import org.jooq.util.maven.example.tables.Conclusion;
import org.jooq.util.maven.example.tables.Condition;
import org.jooq.util.maven.example.tables.Domain;
import org.jooq.util.maven.example.tables.DomainValue;
import org.jooq.util.maven.example.tables.Es;
import org.jooq.util.maven.example.tables.Fact;
import org.jooq.util.maven.example.tables.ReasonTree;
import org.jooq.util.maven.example.tables.Rule;
import org.jooq.util.maven.example.tables.Variable;
import org.jooq.util.maven.example.tables.WorkingMemory;


/**
 * Convenience access to all tables in public
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * Заключения
     */
    public static final Conclusion CONCLUSION = org.jooq.util.maven.example.tables.Conclusion.CONCLUSION;

    /**
     * Условия
     */
    public static final Condition CONDITION = org.jooq.util.maven.example.tables.Condition.CONDITION;

    /**
     * Домен
     */
    public static final Domain DOMAIN = org.jooq.util.maven.example.tables.Domain.DOMAIN;

    /**
     * Все значения доменов
     */
    public static final DomainValue DOMAIN_VALUE = org.jooq.util.maven.example.tables.DomainValue.DOMAIN_VALUE;

    /**
     * Экспертная система
     */
    public static final Es ES = org.jooq.util.maven.example.tables.Es.ES;

    /**
     * Факты
     */
    public static final Fact FACT = org.jooq.util.maven.example.tables.Fact.FACT;

    /**
     * The table <code>public.reason_tree</code>.
     */
    public static final ReasonTree REASON_TREE = org.jooq.util.maven.example.tables.ReasonTree.REASON_TREE;

    /**
     * Правило
     */
    public static final Rule RULE = org.jooq.util.maven.example.tables.Rule.RULE;

    /**
     * Переменная
     */
    public static final Variable VARIABLE = org.jooq.util.maven.example.tables.Variable.VARIABLE;

    /**
     * The table <code>public.working_memory</code>.
     */
    public static final WorkingMemory WORKING_MEMORY = org.jooq.util.maven.example.tables.WorkingMemory.WORKING_MEMORY;
}
