/*
 * This file is generated by jOOQ.
 */
package org.jooq.util.maven.example.tables.records;


import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.TableRecordImpl;
import org.jooq.util.maven.example.tables.WorkingMemory;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class WorkingMemoryRecord extends TableRecordImpl<WorkingMemoryRecord> implements Record2<Long, Long> {

    private static final long serialVersionUID = -1593407182;

    /**
     * Setter for <code>public.working_memory.rule_id</code>.
     */
    public void setRuleId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.working_memory.rule_id</code>.
     */
    @NotNull
    public Long getRuleId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.working_memory.fact_id</code>.
     */
    public void setFactId(Long value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.working_memory.fact_id</code>.
     */
    @NotNull
    public Long getFactId() {
        return (Long) get(1);
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Long, Long> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<Long, Long> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return WorkingMemory.WORKING_MEMORY.RULE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field2() {
        return WorkingMemory.WORKING_MEMORY.FACT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getRuleId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component2() {
        return getFactId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getRuleId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value2() {
        return getFactId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WorkingMemoryRecord value1(Long value) {
        setRuleId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WorkingMemoryRecord value2(Long value) {
        setFactId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WorkingMemoryRecord values(Long value1, Long value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached WorkingMemoryRecord
     */
    public WorkingMemoryRecord() {
        super(WorkingMemory.WORKING_MEMORY);
    }

    /**
     * Create a detached, initialised WorkingMemoryRecord
     */
    public WorkingMemoryRecord(Long ruleId, Long factId) {
        super(WorkingMemory.WORKING_MEMORY);

        set(0, ruleId);
        set(1, factId);
    }
}
