/*
 * This file is generated by jOOQ.
 */
package org.jooq.util.maven.example.tables.records;


import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.util.maven.example.tables.Rule;


/**
 * Правило
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class RuleRecord extends UpdatableRecordImpl<RuleRecord> implements Record4<Long, String, Integer, Long> {

    private static final long serialVersionUID = 1268879075;

    /**
     * Setter for <code>public.rule.id</code>. Идентификатор павила
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.rule.id</code>. Идентификатор павила
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>public.rule.name</code>. Наименование правила
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.rule.name</code>. Наименование правила
     */
    @NotNull
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.rule.order</code>. Порядковый номер правила
     */
    public void setOrder(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.rule.order</code>. Порядковый номер правила
     */
    @NotNull
    public Integer getOrder() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>public.rule.es_id</code>.
     */
    public void setEsId(Long value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.rule.es_id</code>.
     */
    @NotNull
    public Long getEsId() {
        return (Long) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Long, String, Integer, Long> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Long, String, Integer, Long> valuesRow() {
        return (Row4) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return Rule.RULE.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Rule.RULE.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return Rule.RULE.ORDER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field4() {
        return Rule.RULE.ES_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getOrder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long component4() {
        return getEsId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getOrder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value4() {
        return getEsId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleRecord value1(Long value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleRecord value3(Integer value) {
        setOrder(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleRecord value4(Long value) {
        setEsId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RuleRecord values(Long value1, String value2, Integer value3, Long value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached RuleRecord
     */
    public RuleRecord() {
        super(Rule.RULE);
    }

    /**
     * Create a detached, initialised RuleRecord
     */
    public RuleRecord(Long id, String name, Integer order, Long esId) {
        super(Rule.RULE);

        set(0, id);
        set(1, name);
        set(2, order);
        set(3, esId);
    }
}
