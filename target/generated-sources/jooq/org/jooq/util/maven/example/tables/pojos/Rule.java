/*
 * This file is generated by jOOQ.
 */
package org.jooq.util.maven.example.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;


/**
 * Правило
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.11"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Rule implements Serializable {

    private static final long serialVersionUID = -1908867471;

    private Long    id;
    private String  name;
    private Integer order;
    private Long    esId;

    public Rule() {}

    public Rule(Rule value) {
        this.id = value.id;
        this.name = value.name;
        this.order = value.order;
        this.esId = value.esId;
    }

    public Rule(
        Long    id,
        String  name,
        Integer order,
        Long    esId
    ) {
        this.id = id;
        this.name = name;
        this.order = order;
        this.esId = esId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public Integer getOrder() {
        return this.order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @NotNull
    public Long getEsId() {
        return this.esId;
    }

    public void setEsId(Long esId) {
        this.esId = esId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Rule (");

        sb.append(id);
        sb.append(", ").append(name);
        sb.append(", ").append(order);
        sb.append(", ").append(esId);

        sb.append(")");
        return sb.toString();
    }
}
